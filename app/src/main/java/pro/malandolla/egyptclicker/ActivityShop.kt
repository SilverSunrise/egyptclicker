package pro.malandolla.egyptclicker

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import pro.malandolla.egyptclicker.MainActivity.Companion.countMoney
import pro.malandolla.egyptclicker.MainActivity.Companion.money
import kotlinx.android.synthetic.main.activity_shop.*

class ActivityShop : AppCompatActivity() {

    companion object {
        var firstImprovement = 0
        var secondImprovement = 0
        var thirdImprovement = 0
        var fourthImprovement = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shop)
        tv_countShopMoney.text = countMoney.toString()
        shopMoneyUpdate()
        shopOptions()
        shopExit()
    }

    private fun shopExit() {
        btn_exit.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    private fun shopOptions() {
        btn_shopFirstOption.setOnClickListener {
            if (countMoney >= 50) {
                money += 1
                countMoney -= 50
                firstImprovement += 1
                shopMoneyUpdate()
            }
        }

        btn_shopSecondOption.setOnClickListener {
            if (countMoney >= 200) {
                money += 3
                countMoney -= 200
                secondImprovement += 1
                shopMoneyUpdate()
            }
        }

        btn_shopThirdOption.setOnClickListener {
            if (countMoney >= 600) {
                money += 5
                countMoney -= 600
                thirdImprovement += 1
                shopMoneyUpdate()
            }
        }

        btn_shopFourthOption.setOnClickListener {
            if (countMoney >= 1200) {
                money += 10
                countMoney -= 1200
                fourthImprovement += 1
                shopMoneyUpdate()
            }
        }
    }

    private fun shopMoneyUpdate() {
        tv_coinsShopPerClick.text = "Колличество монет за клик: $money"
        btn_shopFirstOption.text = "Деревянный клик \n Приобретён $firstImprovement раз"
        btn_shopSecondOption.text = "Железный клик \n Приобретён $secondImprovement раз"
        btn_shopThirdOption.text = "Золотой клик \n Приобретён $thirdImprovement раз"
        btn_shopFourthOption.text = "Алмазный клик \n Приобретён $fourthImprovement раз"
        tv_countShopMoney.text = countMoney.toString()
    }

    override fun onBackPressed() {
        startActivity(Intent(this, MainActivity::class.java))
        return
    }
}