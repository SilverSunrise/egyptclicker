package pro.malandolla.egyptclicker

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        var countMoney = 0
        var money = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tv_countMoney.text = countMoney.toString()
        addingCurrency()
        showCoinsPerClick()
        openShop()
    }

    private fun showCoinsPerClick() {
        tv_coinsPerClick.text = "Колличество монет за клик: $money"
    }


    private fun addingCurrency() {
        iv_chest.setOnClickListener {
            countMoney += money
            tv_countMoney.text = countMoney.toString()
            chestAnimate()
        }
    }

    private fun openShop() {
        btn_shop.setOnClickListener {
            startActivity(Intent(this, ActivityShop::class.java))
        }
    }

    private fun chestAnimate() {
        iv_chest.animate().apply {
            duration = 40
            scaleX(1.1f)
            scaleY(1.1f)
        }.withEndAction {
            iv_chest.animate().apply {
                duration = 40
                scaleX(1f)
                scaleY(1f)
            }.start()
        }
    }

}